# Projet sixi-ros

Programme ROS pour le bras robotique Sixi 2.

Platforme : NVIDIA Jetson TX2
<br>
Version de ROS utilisée : Noetic

## Description du projet

Le bras robotique Sixi 2 comprend un Arduino MEGA qui se charge du contrôle des moteurs. Les algorithmes plus complexes qui envoient des commandes au microcontrôleur Arduino sont des programmes implémentés en C++ et en Python, utilisés à l'intérieur du framework ROS. Ces algorithmes plus complexes, ce sont entre autres, la cinématique directe et inverse du bras robotique, le contrôle avec la manette, la gestion du feedback des moteurs, etc. Le paquet ROS qui rassemble ces algorithmes est ce qui se trouve dans ce projet.

### Problèmes soulevés avec ce projet en 2021

**Plateforme utilisée**

Pour la CIRC 2021, il avait été convenu que le présent paquet ROS allait être exécuté sur la même Jetson que le paquet ROS de l'équipe Contrôle. Cependant, l'équipe Contrôle utilise ROS Melodic, car il est supporté par Ubuntu 18.04 (qui est la version de Ubuntu sur laquelle Jetpack 4, la version de Linux de la Jetson TX2, est basée). Le présent paquet ROS, destiné au bras robotique, a été programmé avec ROS Noetic, qui ne supporte pas Ubuntu 18.04, mais bien Ubuntu 20.04. Cela n'était pas catastrophique, mais il serait plus sage d'utiliser une plateforme compatible avec ROS Noetic dans le futur, comme un Raspberry Pi Model 4.

**ros_serial**

Ros_serial sert dans ce projet à communiquer avec le Arduino qui permet de communiquer avec les moteurs du bras. Nous avons eu des problèmes à faire fonctionner cette librairie en 2021. Il serait préférable de ne pas l'utiliser, en ne faisant pas communiquer le Raspberry Pi et le Arduino avec des topics ROS, mais seulement avec un bus SPI.

## Structure des fichiers

Section à venir

## Pour travailler sur ce projet

Section à venir
